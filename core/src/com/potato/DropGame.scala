package com.potato

import com.badlogic.gdx.Game

class DropGame extends Game {

  private var world: World = _


  override def create(): Unit = {
    world = new World
    this.setScreen(new MainMenuScreen(world, setScreen))
  }

  override def render(): Unit = {
    super.render()
  }

  override def dispose(): Unit = {
    world.dispose()
  }
}
