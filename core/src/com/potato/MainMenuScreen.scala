package com.potato

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.utils.ScreenUtils
import com.badlogic.gdx.{Gdx, Screen}

class MainMenuScreen(world: World, setScreen: Screen => Unit) extends OnlyRenderScreen {

  private val camera = new OrthographicCamera()
  camera.setToOrtho(false, GlobalConfig.Width, GlobalConfig.Height)

  override def render(delta: Float): Unit = {
    ScreenUtils.clear(0, 0, 0.2f, 1)

    camera.update()
    world.batch.setProjectionMatrix(camera.combined)
    world.batch.begin()
    world.font.draw(world.batch, "Welcome to Drop!", 100, 150)
    world.font.draw(world.batch, "Tap anywhere to begin", 100, 100)
    world.batch.end()

    if (Gdx.input.isTouched()) {
      setScreen(new GameScreen(world))
      dispose()
    }
  }

}
