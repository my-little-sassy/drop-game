package com.potato

object GlobalConfig {
  val Width = 800
  val Height = 480

  object Raindrop {
    val Speed = 200
    val Size = 64
  }

  object Bucket {
    val Size = 64
  }
}
