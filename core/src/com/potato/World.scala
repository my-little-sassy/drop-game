package com.potato

import com.badlogic.gdx.graphics.g2d.{BitmapFont, SpriteBatch}

class World {

  val batch: SpriteBatch = new SpriteBatch()
  val font: BitmapFont = new BitmapFont()

  def dispose(): Unit = {
    font.dispose()
    batch.dispose()
  }
}
