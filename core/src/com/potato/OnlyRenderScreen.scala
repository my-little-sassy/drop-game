package com.potato

import com.badlogic.gdx.Screen

trait OnlyRenderScreen extends Screen{
  override def show(): Unit = {}

  override def resize(width: Int, height: Int): Unit = {}

  override def pause(): Unit = {}

  override def resume(): Unit = {}

  override def hide(): Unit = {}

  override def dispose(): Unit = {}
}
