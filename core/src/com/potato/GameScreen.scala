package com.potato

import com.badlogic.gdx.{Gdx, Input}
import com.badlogic.gdx.graphics.{OrthographicCamera, Texture}
import com.badlogic.gdx.math.{MathUtils, Rectangle, Vector3}
import com.badlogic.gdx.utils.{ScreenUtils, TimeUtils}

class GameScreen(world: World) extends OnlyRenderScreen {

  val dropImage: Texture = new Texture("drop.png")
  val bucketImage: Texture = new Texture("bucket.png")

  val camera: OrthographicCamera = new OrthographicCamera()
  camera.setToOrtho(false, GlobalConfig.Width, GlobalConfig.Height)

  val bucket: Rectangle = new Rectangle()
  bucket.x = GlobalConfig.Width / 2 - GlobalConfig.Bucket.Size / 2
  bucket.y = 20
  bucket.width = GlobalConfig.Bucket.Size
  bucket.height = GlobalConfig.Bucket.Size

  var raindrops: Seq[Rectangle] = Seq.empty
  var lastDropTime: Long = 0


  override def render(delta: Float): Unit = {
    ScreenUtils.clear(0, 0, 0.2f, 1)
    camera.update()

    world.batch.setProjectionMatrix(camera.combined)
    world.batch.begin()
    world.batch.draw(bucketImage, bucket.x, bucket.y)
    raindrops.foreach { raindrop =>
      world.batch.draw(dropImage, raindrop.x, raindrop.y)
    }
    world.batch.end()

    if (Gdx.input.isTouched) {
      val touchPos = new Vector3()
      touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0)
      camera.unproject(touchPos)
      bucket.x = touchPos.x - GlobalConfig.Bucket.Size / 2
    }

    val direction =
      if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) -1
      else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) 1
      else 0
    bucket.x += direction * 200 * Gdx.graphics.getDeltaTime

    if (bucket.x < 0) bucket.x = 0
    if (bucket.x > GlobalConfig.Width - GlobalConfig.Bucket.Size) bucket.x = GlobalConfig.Width - GlobalConfig.Bucket.Size

    if (TimeUtils.nanoTime() - lastDropTime > 1_000_000_000) spawnRaindrop()

    raindrops.foreach { raindrop =>
      raindrop.y -= GlobalConfig.Raindrop.Speed * Gdx.graphics.getDeltaTime
    }
    raindrops = raindrops.filter { raindrop =>
      raindrop.y + GlobalConfig.Raindrop.Size > 0 && !raindrop.overlaps(bucket)
    }

  }

  private def spawnRaindrop(): Unit = {
    val raindrop = new Rectangle()
    raindrop.x = MathUtils.random(0, GlobalConfig.Width - GlobalConfig.Raindrop.Size)
    raindrop.y = GlobalConfig.Height
    raindrop.width = GlobalConfig.Raindrop.Size
    raindrop.height = GlobalConfig.Raindrop.Size
    raindrops = raindrop +: raindrops
    lastDropTime = TimeUtils.nanoTime()
  }

  override def dispose(): Unit = {
    super.dispose()
    dropImage.dispose()
    bucketImage.dispose()
  }
}
